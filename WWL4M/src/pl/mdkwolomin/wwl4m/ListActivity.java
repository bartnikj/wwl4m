package pl.mdkwolomin.wwl4m;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;

import pl.mdkwolomin.wwl4m.R;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SimpleAdapter;
import android.widget.Toast;
import android.support.v4.app.NavUtils;
import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;

public class ListActivity extends Activity {
	
	List<Map<String,String>> lista = new ArrayList<Map<String,String>>();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_list);
		
		ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
		Context context = getApplicationContext();
		
		ListView lv = (ListView)findViewById(R.id.listView1);
		
		MySimpleAdapter sAdapter = new MySimpleAdapter(this, lista, R.layout.activity_list_event,
				new String[]{"title","when"},
				new int[]{R.id.title,R.id.when});
		lv.setAdapter(sAdapter);
		
		ProgressBar pb = (ProgressBar)findViewById(R.id.progressBar1);
		
		//Activity activity = ListActivity.this;
		if (networkInfo != null && networkInfo.isConnected()) {
			try{
				new Kapownik(lista,sAdapter,pb).execute();
			}
			catch(Exception e){
				// coś poszło wyjątkowo źle... URI powyżej jest niepoprawne
			}
		}
		else {
			Toast toast = Toast.makeText(context, "Brak połączenia z internetem", Toast.LENGTH_SHORT);
			toast.show();
		}
		
	}

	/**
	 * Set up the {@link android.app.ActionBar}, if the API is available.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	private void setupActionBar() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			getActionBar().setDisplayHomeAsUpEnabled(true);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		//getMenuInflater().inflate(R.menu.list, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	public void topButtonClicked(View v){
		v.getTag();
	}
	
	class MySimpleAdapter extends SimpleAdapter{
		public MySimpleAdapter(Context context, List<? extends Map<String, ?>> data,
				int resource, String[] from, int[] to){
			super(context, data, resource, from, to);
		}
		@Override
		public View getView(int position, View convertView, final ViewGroup parent) {
			final View view = super.getView(position, convertView, parent);
			final ImageButton ib1 = (ImageButton)view.findViewById(R.id.imageButton1);
			ib1.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					// TODO Obsługa guzika
					Toast toast = Toast.makeText(ListActivity.this, "Wciśnięto1!", Toast.LENGTH_SHORT);
					toast.show();
				}
			});
			final ImageButton ib2 = (ImageButton)view.findViewById(R.id.imageButton2);
			ib2.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					// TODO Obsługa guzika
					Toast toast = Toast.makeText(ListActivity.this, "Wciśnięto2!", Toast.LENGTH_SHORT);
					toast.show();
				}
			});
			
			return view;
		}
	}
	
	class Kapownik extends AsyncTask<Void, Void, Void>{
		
		JSONObject tablica = null;
	    JSONArray jsonArray = null;
	    List<Map<String,String>> lista;
	    SimpleAdapter sa;
	    ProgressBar pb;
		
	    Kapownik(List<Map<String,String>> lista, SimpleAdapter sa, ProgressBar pb){
	    	this.lista = lista;
	    	this.sa = sa;
	    	this.pb = pb;
	    }
	    
	    protected void onPreExecute() {
	    	this.pb.setVisibility(View.VISIBLE);
	    }
	    
		protected Void doInBackground(Void... unused){
			try{
				jsonArray = new JSONArray(readFeed(new URI("http://kulturawpowiecie.info/api?type=json")));
	        } catch (Exception e) {
	            e.printStackTrace();
			}
			return null;
		}
		
		protected void onPostExecute(Void unused) {
			Map<String,String> map;
			for(int i=0; i<jsonArray.length(); i++){
				try{
					tablica = jsonArray.getJSONObject(i);
					map = new HashMap<String,String>();
					map.put("title", tablica.getString("what"));
					map.put("when", tablica.getString("when"));
					lista.add(map);
				}
				catch(Exception e){
					//TODO: catch exception, rename array and objects, rename map
				}
			}
			sa.notifyDataSetChanged();
			//TODO: remove progressbar (look for better solution)
			this.pb.setVisibility(View.GONE);
	    }
		
		private String readFeed(URI uri) {
		    StringBuilder builder = new StringBuilder();
		    HttpClient client = new DefaultHttpClient();
		    HttpGet httpGet = new HttpGet(uri);
		    try {
		        HttpResponse response = client.execute(httpGet);
		        StatusLine statusLine = response.getStatusLine();
		        int statusCode = statusLine.getStatusCode();
		        if (statusCode == 200) {
		            HttpEntity entity = response.getEntity();
		            InputStream content = entity.getContent();
		            BufferedReader reader = new BufferedReader(
		                    new InputStreamReader(content));
		            String line;
		            while ((line = reader.readLine()) != null) {
		                builder.append(line);
		            }
		        } else {
		            Log.e(Kapownik.class.toString(), "Failed to download file");
		        }
		    } catch (ClientProtocolException e) {
		        e.printStackTrace();
		    } catch (IOException e) {
		        e.printStackTrace();
		    }
		    return builder.toString();
		}

	}

}
