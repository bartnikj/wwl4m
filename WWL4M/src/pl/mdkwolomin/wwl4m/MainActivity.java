package pl.mdkwolomin.wwl4m;

import pl.mdkwolomin.wwl4m.R;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		Button button_pobierz = (Button) findViewById(R.id.button_pobierz);
		Button button_kontakt = (Button) findViewById(R.id.button_kontakt);

	    /* akcja po wciśnięciu guzika "lista wydarzeń" */
		button_pobierz.setOnClickListener(new OnClickListener()
	    {
	      public void onClick(View v)
	      {
	    	  Intent aboutIntent = new Intent(MainActivity.this, ListActivity.class);
	    	  startActivity(aboutIntent);
	      }
	    });
	    
		/* akcja po wciśnięciu guzika "lista kontaktów" */
		button_kontakt.setOnClickListener(new OnClickListener()
	    {
	      public void onClick(View v)
	      {
	    	  Intent kontaktIntent = new Intent(MainActivity.this, KontaktListActivity.class);
	    	  startActivity(kontaktIntent);
	      }
	    });
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    // Handle item selection
	    switch (item.getItemId()) {
	        case R.id.action_settings:
	        	Intent prefsIntent = new Intent(this, SettingsActivity.class);
	        	startActivity(prefsIntent);
	            return true;
	        case R.id.action_about:
	        	Intent aboutIntent = new Intent(this, MainAboutActivity.class);
	        	startActivity(aboutIntent);
	            return true;
	        default:
	            return super.onOptionsItemSelected(item);
	    }
	}

}
